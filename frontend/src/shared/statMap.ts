export const statMap = {
    hp: 'HP',
    speed: 'Spe',
    attack: 'Atk',
    defense: 'Def',
    'special-attack': 'SpA',
    'special-defense': 'SpD'
  }