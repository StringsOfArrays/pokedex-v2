export enum Gen {
    KANTO = 'Kanto',
    JOHTO = 'Johto',
    HOENN = 'Hoenn',
    SINNOH = 'Sinnoh',
    UNOVA = 'Unova',
    KALOS = 'Kalos',
    ALOLA = 'Alola',
    GALAR = 'Galar',
    PALDEA = 'Paldea'
}