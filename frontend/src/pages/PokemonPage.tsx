import { Grid } from "@mui/material";
import PokemonDetail from "../components/PokemonDetail";
import PokemonList from "../components/PokemonList";
import { useEffect, useState } from "react";
import { Filters } from "../shared/filters";
import { SortBy } from "../shared/sortBy";

const PokemonPage: React.FunctionComponent<{
  filters: Array<{key: Filters, value: string}>,
  sortBy: SortBy | undefined
}> = ({filters, sortBy}) => {
  useEffect(() => {
    const persistedId = localStorage.getItem('selectedPokemon');
    if(persistedId) {
      setId(persistedId);
    };
  }, []);

  const [selectedId, setId] = useState<string>("");
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={6}>
        <PokemonList 
        setId={setId} 
        filters={filters} 
        sortBy={sortBy}></PokemonList>
      </Grid>
      <Grid item xs={12} md={6}>
        <PokemonDetail id={selectedId}></PokemonDetail>
      </Grid>
    </Grid>
  );
};

export default PokemonPage;
