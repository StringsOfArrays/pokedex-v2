import { getBackground } from "./getBackground";

describe('getBackground', () => {
    it('returns a background for a valid region', () => {
        const givenRegion = "alola";
        const result = getBackground(givenRegion);
        expect(result).not.toBe("");
        expect(result).not.toBe(null);
        expect(result).not.toBe(undefined);
        expect(typeof result).toBe(typeof "");
    });

    it('fails silently by returning an empty string for invalid inputs', () => {
        const givenRegion = "";
        const result = getBackground(givenRegion);
        expect(result).toBe("");
        expect(result).not.toBe(null);
        expect(result).not.toBe(undefined);
        expect(typeof result).toBe(typeof "");
    })
});