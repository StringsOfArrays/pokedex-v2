export const getBackground = (region: string | null | undefined) => {
    if(!region || region === "Unknown") return "";
    return `url(${require(`../../public/images/Regions/${region.toLowerCase()}.jpg`)})`;
};
