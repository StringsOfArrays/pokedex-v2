import { getColors } from "./getColors";

describe('getColors', () => {
    it('throws an error if the given type does not exist', () => {
        const givenTypes = ['test'];
        const expectedError = new Error('One or more given types is not a color mappable type');
        expect(() => getColors(givenTypes)).toThrow(expectedError);
    });

    it('returns two times the same color for one given type', () => {
        const givenTypes = ['normal'];
        const expectedColor = '#b6b6a8';
        const result = getColors(givenTypes);
        expect(result.color1).toEqual(expectedColor);
        expect(result.color2).toEqual(expectedColor);
    });

    it('returns two different colors for two given types', () => {
        const givenTypes = ['normal', 'fire'];
        const expectedColor1 = '#b6b6a8';
        const expectedColor2 = '#fa5643';
        const result = getColors(givenTypes);
        expect(result.color1).toEqual(expectedColor1);
        expect(result.color2).toEqual(expectedColor2);
    });

    it('throws an error if the input list is longer than two valid entries', () => {
        const givenTypes = ['normal', 'fire', 'flying'];
        const expectedError = new Error('Pokemon must not have more than two types!');
        expect(() => getColors(givenTypes)).toThrow(expectedError);
    });
});