import { getTypes } from "./getTypes";

describe('getTypes', () => {
    it('returns en empty list if no or an empty list is provided', () => {
        expect(getTypes(null)).toEqual([]);
        expect(getTypes(undefined)).toEqual([]);
        expect(getTypes([])).toEqual([]);
    });

    it('returns null for invalid types', () => {
        expect(getTypes(['invalid'])).toEqual([null]);
    });

    it('returns a type icon for valid types', () => {
        expect(getTypes(['normal'])).toEqual(['normal.png']);
    })
});