import { getGenAndRegionById } from "./getGenAndRegion";

describe('getGenAndRegion', () => {
    it('returns null if no id is provided', () => {
        expect(getGenAndRegionById(null)).toEqual(null);
        expect(getGenAndRegionById(undefined)).toEqual(null);
    });

    it('returns unknown region for out of bound ids', () => {
        expect(getGenAndRegionById('2000')).toEqual({ gen: 0, region: "Unknown" });
    });

    it('returns correct region for given valid id', () => {
        expect(getGenAndRegionById('1')).toEqual({ gen: 1, region: "Kanto" });
    })
});