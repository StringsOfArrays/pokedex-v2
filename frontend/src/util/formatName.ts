export const formatName = (name: string | null | undefined) => {
    if(!name) {
        throw new Error('No name given');
    }
    return name.charAt(0).toUpperCase() + name.slice(1);
}