export const getTypes = (types: string[] | null | undefined) => {
    if(!types) {
        return [];
    }
    return types.map(type => {
        try {
            return  require(`../../public/images/typeIcons/${type.toLowerCase()}.png`);
        }
        catch {
            return null;
        }
    });
}