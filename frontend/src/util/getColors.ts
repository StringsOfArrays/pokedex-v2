const colorMap: Record<string, string> = {
    normal: "#b6b6a8",
    poison: "#9a5692",
    psychic: "#f261ae",
    grass: "#8cd751",
    ground: "#eccb56",
    ice: "#96f1ff",
    fire: "#fa5643",
    rock: "#cdbc72",
    dragon: "#8774ff",
    water: "#57b0ff",
    bug: "#c3d21f",
    dark: "#856451",
    fighting: "#a85642",
    ghost: "#7773d3",
    steel: "#c4c2db",
    flying: "#79a4ff",
    electric: "#fde53c",
    fairy: "#fde53c",
};

export const getColors = (types: string[]) => {
    const onlyExistingTypes = types.every(type => !!colorMap[type]);
    if(!onlyExistingTypes) throw new Error('One or more given types is not a color mappable type');

    switch(types.length) {
        case 0:
            throw new Error("Types must not be empty!");
        case 1:
            return {color1: colorMap[types[0]], color2: colorMap[types[0]]};
        case 2:
            return {color1: colorMap[types[0]], color2: colorMap[types[1]]};
        default:
            throw new Error("Pokemon must not have more than two types!");
    }
}