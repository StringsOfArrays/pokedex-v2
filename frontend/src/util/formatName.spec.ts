import { formatName } from "./formatName";

describe('formatName', () => {
    it('should transform lowercase words to have a leading uppercase letter', () => {
        const givenName = "test";
        const expectedName = formatName(givenName);
        expect(expectedName).toEqual("Test");
    });

    it('should be able to handle invalid inputs', () => {
        const givenName = null;
        const expectedError = new Error('No name given');
        expect(() => formatName(givenName)).toThrow(expectedError);
    });
});