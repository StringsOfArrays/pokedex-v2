export const getGenAndRegionById = (pokeId: string | null | undefined) => {
    if(!pokeId) {
        return null;
    }
    const id = Number(pokeId);
    if (id <= 151) return { gen: 1, region: "Kanto" };
    if (id <= 251) return { gen: 2, region: "Johto" };
    if (id <= 386) return { gen: 3, region: "Hoenn" };
    if (id <= 493) return { gen: 4, region: "Sinnoh" };
    if (id <= 649) return { gen: 5, region: "Unova" };
    if (id <= 721) return { gen: 6, region: "Kalos" };
    if (id <= 809) return { gen: 7, region: "Alola" };
    if (id <= 893) return { gen: 8, region: "Galar" };
    if (id <= 1008) return { gen: 9, region: "Paldea"};
    return { gen: 0, region: "Unknown" };
};