import React, { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import styled from 'styled-components';
import Navigation from "./components/Navigation";
import ErrorPage from './pages/ErrorPage';
import PokemonPage from './pages/PokemonPage';
import '@fontsource/montserrat/300.css';
import '@fontsource/montserrat/400.css';
import '@fontsource/montserrat/500.css';
import '@fontsource/montserrat/700.css';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { Filters } from './shared/filters';
import { SortBy } from './shared/sortBy';

const theme = createTheme({
  components: {
    MuiTypography: {
      defaultProps: {
        variantMapping: {
          h1: 'h1',
          h2: 'h2',
          h3: 'h3',
          h4: 'h4',
          h5: 'h5',
          h6: 'h6',
          subtitle1: 'h2',
          subtitle2: 'h3',
          body1: 'span',
          body2: 'p',
        },
      },
    },
    MuiPaper: {
      defaultProps: {
        style: {
          padding: '.5rem',
          background: 'rgba(255, 255, 255, .7)'
        }
      }
    }
  },
});


const Layout = styled.main`
  padding: 2rem;
`;

const PageWrapper =  styled.div`
  min-height: 40rem;
`;

const App: React.FunctionComponent = () => {
  const [filters, setFilters] = useState<Array<{key: Filters, value: string}>>([]);
  const [sortBy, setSortBy] = useState<SortBy>();

  return (
    <>
      <ThemeProvider theme={theme}>
        <Navigation setFilters={setFilters} setSortBy={setSortBy}/>
        <Layout>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={
                <PageWrapper>
                  <PokemonPage filters={filters} sortBy={sortBy}/>
                </PageWrapper>
              } />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </BrowserRouter>
        </Layout>
      </ThemeProvider>
    </>
  );
}

export default App;
