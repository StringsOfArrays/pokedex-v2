import { MockedProvider } from "@apollo/client/testing";
import { render, screen, waitFor } from "@testing-library/react"
import PokemonList from "./PokemonList";
import { getAllPokemonMocks } from "../graphql/queries/testing/mocks";
import { SortBy } from "../shared/sortBy";
import { Filters } from "../shared/filters";

describe('PokemonList', () => {
    it('renders', async () => {
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={jest.fn()} filters={[]} sortBy={null}/>
            </MockedProvider>
        );
        expect(await screen.getByTestId('pokemon-list')).toBeInTheDocument();
    });
    
    it('can select pokemon and persist selection', async () => {
        const setIdMock = jest.fn();
    
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={setIdMock} filters={[]} sortBy={null}/>
            </MockedProvider>
        );
    
        await waitFor(() => {
            expect(screen.getByTestId('listitem-a')).toBeInTheDocument();
        });
    
        const listElement = await screen.getByTestId('listitem-a');
        listElement.click();
    
        expect(setIdMock).toBeCalled();
        expect(localStorage.getItem('selectedPokemon')).toBe('1');
    });

    it('can sort by dex number', async () => {
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={jest.fn()} filters={[]} sortBy={SortBy.DEX_NO}/>
            </MockedProvider>
        );

        await waitFor(() => {
            expect(screen.getAllByTestId('listitem', {exact: false}).length).toBe(4);;
        });

        const listElements = await screen.getAllByTestId('listitem', {exact: false});
        expect(listElements[0]).toHaveTextContent("A");
        expect(listElements[1]).toHaveTextContent("C");
        expect(listElements[2]).toHaveTextContent("D");
        expect(listElements[3]).toHaveTextContent("B");
    });

    it('can sort by alphabet', async () => {
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={jest.fn()} filters={[]} sortBy={SortBy.ALPHABETICAL}/>
            </MockedProvider>
        );

        await waitFor(() => {
            expect(screen.getAllByTestId('listitem', {exact: false}).length).toBe(4);;
        });

        const listElements = await screen.getAllByTestId('listitem', {exact: false});
        expect(listElements[0]).toHaveTextContent("A");
        expect(listElements[1]).toHaveTextContent("B");
        expect(listElements[2]).toHaveTextContent("C");
        expect(listElements[3]).toHaveTextContent("D");
    });

    it('can filter by type', async () => {
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={jest.fn()} filters={[{key: Filters.BY_TYPE, value: 'poison'}]} sortBy={null}/>
            </MockedProvider>
        );

        await waitFor(() => {
            expect(screen.getAllByTestId('listitem', {exact: false}).length).toBe(1);;
        });

        const listElements = await screen.getAllByTestId('listitem', {exact: false});
        expect(listElements[0]).toHaveTextContent("B");
    });

    it('can filter by gen', async () => {
        render(
            <MockedProvider mocks={getAllPokemonMocks}>
                <PokemonList setId={jest.fn()} filters={[{key: Filters.BY_GEN, value: 'Kanto'}]} sortBy={null}/>
            </MockedProvider>
        );

        await waitFor(() => {
            expect(screen.getAllByTestId('listitem', {exact: false}).length).toBe(2);;
        });

        const listElements = await screen.getAllByTestId('listitem', {exact: false});
        expect(listElements[0]).toHaveTextContent("A");
        expect(listElements[1]).toHaveTextContent("C");
    });
});
