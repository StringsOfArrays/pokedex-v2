import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
} from 'chart.js';
import { Radar } from 'react-chartjs-2';
import { Maybe, Stat } from '../graphql/generated/graphql';
import { statMap } from '../shared/statMap';

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
);

const StatChart: React.FunctionComponent<{stats: Maybe<Maybe<Stat>[]> | undefined}> = ({stats}) => {
    if(!stats) {
        return (
            <div>Error! No Stats Provided!</div>
        );
    }
    
    const labels = stats.map(stat => {
      const key = stat!.name as keyof typeof statMap;
      return statMap[key];
    });
    const dataPoints = stats.map(stat => stat!.value);
    const data = {
        labels,
        datasets: [
            {
              data: [1, 255, 255, 255, 255, 255],
              backgroundColor: 'rgba(255, 99, 132, 0)',
              borderColor: 'rgba(255, 99, 132, 0)',
              pointRadius: -1
            },
            {
              data: dataPoints,
              backgroundColor: '#fd779489',
              borderColor: '#ff416a',
              borderWidth: 1,
              pointRadius: .5
            },
          ],
    }

    return <Radar data={data} 
                  data-testid="statchart"
                  options={
                      { 
                        scales: {
                          r: {
                            ticks: {
                                display: false,
                                maxTicksLimit: 10
                            },
                            grid: {
                              color: 'rgba(0,0,0,.5)'
                            },
                            angleLines: {
                              color: 'rgba(0,0,0,.5)'
                            }
                          }
                        }
                      }
                    }/>;
}

export default StatChart;