import { Button, FormControl, Grid, InputLabel, MenuItem, Select, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React, { useState } from 'react';
import styled from "styled-components";
import { Filters } from '../shared/filters';
import { Gen } from '../shared/gen';
import { SortBy } from '../shared/sortBy';
import { allTypes } from '../shared';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: 1,
      minWidth: 120,
      backgroundColor: "white",
      "@media(max-width: 960px)": {
        width: "200px",
      },
    },
    selectEmpty: {
      marginTop: 2,
    },
    sortBtn: {
      background: "white",
      "@media(max-width: 960px)": {
        width: "200px",
        height: "80px",
      },
    },
    menuBtn: {
      background: "white",
    },
    commandLabel: {
      "@media(max-width: 960px)": {
        fontSize: 20,
        fontWeight: "bold",
      },
    },
  }));

const NavWrapper = styled.nav`
    height: 5rem;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-items: center;
    background-color: white;
    box-shadow: 0 8px 6px -6px black;
`;

const MenuButtonWrapper = styled.div<{ isOpen: boolean }>`
  position: absolute;
  right: 5%;
  top: 1rem;
  z-index: 3;
  @media (max-width: 960px) {
    right: 10%;
    position: ${(props) => (props.isOpen ? "fixed" : "absolute")};
  }
  @media (max-width: 600px) {
    right: 5%;
  }
`;

const Banner = styled.div<{ isVisible: boolean }>`
  display: ${(props) => (props.isVisible ? "block" : "none")};
  position: absolute;
  height: 5rem;
  width: 100%;
  background-color: white;
  top: 0;
  z-index: 2;
  @media (max-width: 960px) {
    height: 100vh;
    position: fixed;
    background-color: rgba(255, 255, 255, 0.9);
  }
`;

const Aligner = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 960px) {
    flex-direction: column;
  }
`;

const ResponsiveFlexSpacer = styled.div<{
    width: string;
    responsiveHeight: string;
  }>`
    height: 100%;
    width: ${(props) => props.width};
    @media (max-width: 960px) {
      width: 100%;
      height: ${(props) => props.responsiveHeight};
    }
  `;

const gens: Gen[]  = Object.values(Gen);

const Navigation: React.FunctionComponent<{
    setFilters: React.Dispatch<React.SetStateAction<{
      key: Filters;
      value: string;
    }[]>>,
    setSortBy: React.Dispatch<React.SetStateAction<SortBy | undefined>>
  }> = ({setFilters, setSortBy}) => {
        
    const classes = useStyles();
    const [isVisible, setIsVisible] = useState(false);
    const [type, setType] = useState("none");
    const [gen, setGen] = useState<Gen | "none">("none");

    return (
        <header data-testid="header">
            <NavWrapper>
                <MenuButtonWrapper isOpen={isVisible}>
                    <Button
                    data-testid="menu-toggle"
                    variant="contained"
                    onClick={() => setIsVisible(!isVisible)}
                    >
                    {isVisible ? "Hide" : "Show"} Menu
                    </Button>
                </MenuButtonWrapper>
                <Banner isVisible={isVisible}>
                    <Grid container style={{ height: "100%" }} alignItems="center">
                    <Grid item xs={undefined} md={2} />
                    <Grid item md={4} lg={3} xs={12}>
                        <Aligner>
                        <Typography className={classes.commandLabel}>Sort by</Typography>
                        <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
                        <Button
                            data-testid="sort-by-id"
                            variant="contained"
                            onClick={() => {setSortBy(SortBy.DEX_NO)}}
                            className={classes.sortBtn}
                        >
                            Dex No.
                        </Button>
                        <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
                        <Button
                            data-testid="sort-by-name"
                            variant="contained"
                            onClick={() => {setSortBy(SortBy.ALPHABETICAL)}}
                            className={classes.sortBtn}
                        >
                            A - Z
                        </Button>
                        </Aligner>
                    </Grid>
                    <Grid item md={2} xs={12}>
                        <Aligner>
                        <Typography className={classes.commandLabel}>
                            Filter by
                        </Typography>
                        <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel>Type</InputLabel>
                            <Select
                            value={type}
                            onChange={(e) => {
                                setType(() => e.target.value);
                                if(e.target.value === "none") {
                                    setFilters(state => state.filter(item => item.key !== Filters.BY_TYPE));
                                } else {
                                    setFilters(state => [...state.filter(item => item.key !== Filters.BY_TYPE), {
                                        key: Filters.BY_TYPE,
                                        value: e.target.value
                                    }]);
                                }
                            }}
                            label="Type"
                            >
                            <MenuItem value={"none"} onClick={() => {}}>
                                None
                            </MenuItem>
                            {allTypes.map((type) => (
                                <MenuItem
                                key={`${type}-type`}
                                onClick={() => {}}
                                value={type}
                                >
                                {type.charAt(0).toUpperCase() + type.slice(1)}
                                </MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                        </Aligner>
                    </Grid>
                    <Grid item md={2} xs={12}>
                        <Aligner>
                        <Typography className={classes.commandLabel}>
                            Filter by
                        </Typography>
                        <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel>Gen</InputLabel>
                            <Select
                            value={gen}
                            onChange={(e) => {
                                setGen(() => e.target.value as Gen);
                                if(e.target.value === "none") {
                                    setFilters(state => state.filter(item => item.key !== Filters.BY_GEN));
                                } else {
                                    setFilters(state => [...state.filter(item => item.key !== Filters.BY_GEN), {
                                        key: Filters.BY_GEN,
                                        value: e.target.value
                                    }]);
                                }
                            }}
                            label="Type"
                            >
                            <MenuItem value={"none"} onClick={() => {}}>
                                None
                            </MenuItem>
                            {gens.map((gen) => (
                                <MenuItem
                                key={`${gen}-type`}
                                onClick={() => {}}
                                value={gen}
                                >
                                {gen}
                                </MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                        </Aligner>
                    </Grid>
                    </Grid>
                </Banner>
            </NavWrapper>
        </header>
    );
}
export default Navigation;