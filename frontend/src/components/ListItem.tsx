import { Avatar, Box, Typography } from "@mui/material";
import styled from "styled-components";
import { getColors } from "../util";

const ItemWrapper = styled.div<{
    backgroundColor1: string; 
    backgroundColor2: string
    }>`
        border-radius: .5rem;
        background: linear-gradient(25deg, ${(props) => (props.backgroundColor1)} 50%, ${(props) => (props.backgroundColor2)} 50%);
        padding: 1rem;
        margin: .5rem 1rem 1rem 0;
        &:hover {
            transition: scale .2s ease-in-out;
            transform: scale(.98);
            cursor: pointer;
            box-shadow: inset 0 0 5px black;
        }
`;

const itemGrid = {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
};

const gridItem = {
    display: 'flex', 
    justifyContent: 'center'
};

const ListItem: React.FunctionComponent<{
    types: string[];
    spriteUrl: string;
    name: string;
    dexNo: string;
    onClick: (...params: any) => any
}> = ({types, spriteUrl, name, dexNo, onClick}) => {
    const {color1, color2} = getColors(types);
    return(
        <ItemWrapper data-testid={`listitem-${name}`} backgroundColor1={color1} backgroundColor2={color2} onClick={() => {onClick()}}>
            <Box sx={itemGrid}>
                <Box sx={gridItem}>
                    <Avatar sx={{height: '5rem', width: '5rem'}} src={spriteUrl} alt={`Picture of the Pokemon ${name}`} />
                </Box>
                <Box sx={gridItem}>
                    <Typography fontWeight="bold" variant="body1">{name.toUpperCase()}</Typography>
                </Box>
                <Box sx={gridItem}>
                    <Typography fontWeight="bold" variant="body1">#{dexNo}</Typography>
                </Box>
            </Box>
        </ItemWrapper>
    );
}

export default ListItem;