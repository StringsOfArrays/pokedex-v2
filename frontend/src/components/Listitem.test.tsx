import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import ListItem from './ListItem';

const dummyprops = {
    types: ['normal'],
    spriteUrl: '',
    name: 'testmon',
    dexNo: '1',
    onClick: () => {}
}

describe('ListItem', () => {
  it('renders', () => {
    render(<ListItem {...dummyprops} />);
    const wrapper = screen.getByTestId(`listitem-${dummyprops.name}`);
    expect(wrapper).toBeInTheDocument();
  });
  
  it('fires onClick when clicking it', () => {
      const onClickSpy = jest.spyOn(dummyprops, 'onClick');
      render(<ListItem {...dummyprops} />);
      const wrapper = screen.getByTestId(`listitem-${dummyprops.name}`);
      fireEvent.click(wrapper);
      expect(onClickSpy).toHaveBeenCalled();
  });
});