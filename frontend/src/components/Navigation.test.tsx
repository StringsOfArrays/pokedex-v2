import { fireEvent, render, screen } from "@testing-library/react";
import Navigation from "./Navigation";
import { useState } from "react";
import { Filters } from "../shared/filters";
import { SortBy } from "../shared/sortBy";
import React from "react";

const TestComponent = () => {
    const [filters, setFilters] = useState<Array<{key: Filters, value: string}>>([]);
    const [sortBy, setSortBy] = useState<SortBy>();

    return (<Navigation setFilters={setFilters} setSortBy={setSortBy}/>);
}

describe('Navigation', () => {
    it('renders', () => {
        render(<TestComponent/>);
        const header = screen.getByTestId('header');
        expect(header).toBeInTheDocument();
    });

    it('can toggle its menu', () => {
        render(<TestComponent/>);
        const sortButton = screen.getByTestId('sort-by-id');
        expect(sortButton).not.toBeVisible();
        const menuButton = screen.getByTestId('menu-toggle');
        fireEvent.click(menuButton);
        expect(sortButton).toBeVisible();
    });
    
    it('can accept the input command to sort by id or name', () => {
        const setFiltersMock = jest.fn();
        const setSortByMock = jest.fn();
      
        render(<Navigation setFilters={setFiltersMock} setSortBy={setSortByMock} />);
      
        const sortByIdButton = screen.getByTestId('sort-by-id');
        const sortByNameButton = screen.getByTestId('sort-by-name');
        fireEvent.click(sortByIdButton);
        fireEvent.click(sortByNameButton);
      
        expect(setSortByMock).toHaveBeenCalledWith(SortBy.DEX_NO);
        expect(setSortByMock).toHaveBeenCalledWith(SortBy.ALPHABETICAL);
    });
});

