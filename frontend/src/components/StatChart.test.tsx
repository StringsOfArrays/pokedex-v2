import { render, screen } from "@testing-library/react";
import StatChart from "./StatChart";
import { Stat } from "../graphql/generated/graphql";

describe('StatChart', () => {
    it('renders with Error if no stat list is provided', () => {
        render(<StatChart stats={undefined}/>);
        expect(screen.getByText("Error! No Stats Provided!")).not.toBeFalsy();
    });

    it('renders a given list of stats', () => {
        const stats: Stat[] = [{__typename: 'Stat', name: 'test-state', value: 1}];
        render(<StatChart stats={stats}/>);
        expect(screen.getByTestId("statchart")).not.toBeFalsy();
    });
});