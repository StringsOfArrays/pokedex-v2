import { useQuery } from "@apollo/client";
import { Box, CircularProgress, Typography } from "@mui/material";
import ListItem from "./ListItem";
import { FixedSizeList as List } from "react-window";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import { Filters } from "../shared/filters";
import { Pokemon } from "../graphql/generated/graphql";
import { Gen } from "../shared/gen";
import { SortBy } from "../shared/sortBy";
import { getAllPokemon } from "../graphql/queries/pokemonQueries";

const PokemonList: React.FunctionComponent<{
    setId: Dispatch<SetStateAction<string>>,
    filters: Array<{key: Filters, value: string}>,
    sortBy: SortBy | undefined | null
}> 
= ({setId, filters, sortBy}) => {
    const filterByType = (pokemon: Pokemon, type: string) => {
        return pokemon.types!.includes(type);
    }

    const filterByGen = (pokemon: Pokemon, gen: string) => {
        const selectedGen = gen as Gen;
        const dexNo = Number(pokemon.id);
        switch(selectedGen) {
            case Gen.KANTO: return dexNo <= 151;
            case Gen.JOHTO: return dexNo > 151 && dexNo <= 251;
            case Gen.HOENN: return dexNo > 251 && dexNo <= 386;
            case Gen.SINNOH: return dexNo > 386 && dexNo <= 493;    
            case Gen.UNOVA: return dexNo > 493 && dexNo <= 649;
            case Gen.KALOS: return dexNo > 649 && dexNo <= 721;
            case Gen.ALOLA: return dexNo > 721 && dexNo <= 809;
            case Gen.GALAR: return dexNo > 809 && dexNo <= 893;
            case Gen.PALDEA: return dexNo > 893 && dexNo <= 1008;
            default: return true;
        }
    }

    const updateFilteredPokemon = (list: Pokemon[] | null = null) => {
        const listToFilter = list ?? allPokemon;
        const filteredList = listToFilter.filter(pokemon => {
           return filters.every(filter => {
                switch(filter.key) {
                    case Filters.BY_TYPE:
                        return filterByType(pokemon, filter.value)
                    case Filters.BY_GEN:
                        return filterByGen(pokemon, filter.value);
                    default: 
                        return true;
                }
           });
        });

        return sortFilteredPokemon(filteredList);
    };

    const sortFilteredPokemon = (list: Pokemon[]) => {
        if(!sortBy) return list;
        switch(sortBy) {
            case SortBy.ALPHABETICAL: 
                return list.sort((a, b) => {
                    const leftName = a.name;
                    const rightName = b.name;
                    if(leftName < rightName) return -1;
                    if(leftName > rightName) return 1;
                    return 0;
                });
            case SortBy.DEX_NO:
                return list.sort((a, b) => {
                    const leftIndex = Number(a.id);
                    const rightIndex = Number(b.id);
                    return leftIndex - rightIndex;
                });
            default: 
                return list;
        }
    }

    useEffect(() => {
       const newList = updateFilteredPokemon();
       setFilteredPokemon(() => newList);
    }, [filters, sortBy]);

    const [filteredPokemon, setFilteredPokemon] = useState<Pokemon[]>([]);
    const [allPokemon, setAllPokemon] = useState<Pokemon[]>([]);

    const {loading, error} = useQuery(getAllPokemon, {
        onCompleted: (data) => {
            const allPokemon: Pokemon[] = data.getAllPokemon;
            setAllPokemon(() => allPokemon);
            setFilteredPokemon(() => JSON.parse(JSON.stringify(updateFilteredPokemon(allPokemon))));
        }, 
        onError: (error) => console.log(error)
    });

    const virtualizeList = ({index, style} : any) => {
        const pokemon = filteredPokemon[index] ?? [];
        const onClick = () => {
            setId(pokemon.id);
            localStorage.setItem('selectedPokemon', pokemon.id);
        }
        return (
            <div style={style}>
            <ListItem 
                key={pokemon.name}
                types={pokemon.types as string[]}
                spriteUrl={pokemon.sprite!}
                name={pokemon.name}
                dexNo={pokemon.id}
                onClick={onClick}/>
        </div>
        );
    };

    return (
        <Box data-testid="pokemon-list" sx={{display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '45.5rem'}}>
            {(loading && !error) ? 
            <CircularProgress/> 
            :
            <List 
                height={735}
                width="100%"
                itemSize={140}
                itemCount={filteredPokemon.length}
            >
               {virtualizeList}
            </List>
            }
            {error && <Typography>An Error occured</Typography>}
        </Box>
    );
}

export default PokemonList;