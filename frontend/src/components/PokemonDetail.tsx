import { useQuery } from "@apollo/client";
import { Avatar, Box, CircularProgress, Grid, Paper, Typography, useMediaQuery, useTheme } from "@mui/material";
import { useState } from "react";
import styled from "styled-components";
import { ExtendedPokemon } from "../graphql/generated/graphql";
import StatChart from "./StatChart";
import { statMap } from "../shared/statMap";
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import { formatName, getBackground, getGenAndRegionById, getTypes } from "../util";
import { getPokemon } from "../graphql/queries/pokemonQueries";

const PokemonDetailFrame = styled.div<{background: string}>`
    background: ${(props) => props.background ?? 'white'};
    background-position: center;
    background-size: cover;
    border-radius: .5rem;
    padding: 1rem;
    min-height: 43.5rem;
    border: 1px solid black;
`;

const LoadWrapper = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const TypeImg = styled.img<{mb?: number}>`
    width: 5rem;
    display: block;
    margin-bottom: ${(props) => props.mb ?? 0}rem;
`;

const Square = styled.div<{color: string}>`
    display: inline-block;
    height: 1rem;
    width: 1rem;
    border-radius: .2rem;
    background-color: ${(props) => props.color};
`;

const PokemonDetail: React.FunctionComponent<{id: string | null}> = ({id}) => {
    const [pokemon, setPokemon] = useState<ExtendedPokemon>();
    const {loading, error, data} = useQuery(getPokemon, {
        variables: {getPokemonId: Number(id)},
        skip: !id,
        onCompleted(data) {
            setPokemon(data.getPokemon);
        },
    });
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("lg"));
    const useShorthand = useMediaQuery('(max-width:1500px');
    const origin = getGenAndRegionById(pokemon?.pokemon.id);
    const types = getTypes(pokemon?.pokemon.types as string[]);
    const displayState = () => {
        if(loading) {
            return (
            <LoadWrapper>
                <CircularProgress />
            </LoadWrapper>)
        }

        if(!pokemon) {
            return (
            <LoadWrapper>
                <h2>No Data</h2>
                <p>Click on a Pokemon to learn more</p>
            </LoadWrapper>)
        }

        if(error) {
            return (
            <LoadWrapper>
                <h2>An Error Occured</h2>
            </LoadWrapper>)
        }

        return null;
    };

    return (
        <PokemonDetailFrame data-testid="detail-frame" background={getBackground(origin?.region)}>
            {
                displayState() 
                ?? 
                (pokemon && <>
                    <Paper>
                        {
                            isMobile && 
                            <Typography 
                                fontWeight="bold" 
                                textAlign="center" 
                                variant="subtitle1"
                                data-testid="pokedex-no">
                                    No. {pokemon.pokemon.id}
                            </Typography>
                        }
                        <Typography 
                            textAlign="center" 
                            variant={isMobile ? "h3" : "h2"} 
                            fontWeight="bold"
                            data-testid="pokedex-no">
                                {!isMobile && `No. ${pokemon!.pokemon.id} - `} {formatName(pokemon!.pokemon.name)}
                        </Typography>
                    </Paper>
                    
                    <Grid container mt={1} spacing={2}>
                        <Grid item xs={12} lg={6}>
                            <Paper sx={{display: 'flex', 
                                        flexDirection: 'row',
                                        justifyContent: 'space-evenly'}}>
                                <Avatar 
                                    sx={{height: '10rem', width: '10rem', background: 'rgba(255, 255, 255, .5)'}} 
                                    src={pokemon.pokemon.sprite as string} 
                                    alt={`An image of the Pokemon ${pokemon.pokemon.name}`}/>
                                <Box sx={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                                    <Typography variant="body1" fontWeight="bold" mb={2}>Types:</Typography>
                                    {
                                        types.map((typeImg, index) =>
                                            <TypeImg 
                                                key={pokemon!.pokemon.types![index]}
                                                src={typeImg} 
                                                alt={`An Icon of the Type ${pokemon!.pokemon.types![index]}`} 
                                                mb={index === 0 ? .1 : 0}
                                                data-testid={pokemon!.pokemon.types![index]}/>
                                        )
                                    }
                                </Box>
                            </Paper>
                        </Grid>
                        {
                            !isMobile &&
                            <Grid item xs={12} lg={6}>
                                <Paper sx={{display: 'grid', gridTemplateColumns: '1fr 1fr', alignItems: "center"}}>
                                    <Box sx={{height: '10rem'}}>
                                        <StatChart stats={pokemon.stats}></StatChart>
                                    </Box>
                                    <Box sx={{maxHeight: '10rem'}}>
                                        {
                                            pokemon.stats 
                                            && 
                                            pokemon.stats.map(
                                                stat =>
                                                <Typography key={stat?.name} sx={{display: "block"}} variant="body1" fontWeight="bold">
                                                    {useShorthand ? statMap[stat?.name as keyof typeof statMap] : formatName(stat?.name as string)}: <Typography variant="body1">{stat?.value}</Typography>
                                                </Typography>
                                            )
                                        }
                                    </Box>
                                </Paper>
                            </Grid>
                        }
                    </Grid>

                    <Box mt={3}>
                        <Paper>
                            <Typography variant="h5" fontWeight="bold">
                                Description:
                            </Typography>
                            <Typography variant="body2" data-testid="description">
                                {pokemon.description?.replaceAll('\f', '')}
                            </Typography>

                            <Typography variant="h6" fontWeight="bold" mt={2}>
                                Appearance:
                            </Typography>
                            <Typography variant="body2" data-testid="weight">
                                Weight - {pokemon.weight}lbs
                            </Typography>
                            <Typography variant="body2" data-testid="height">
                                Height - {pokemon.height! / 10}m
                            </Typography>
                            <Typography variant="body2">
                                Color:
                            </Typography>
                            <Box sx={{display: 'flex', alignItems: "center"}}>
                                <Square color={pokemon.color!} />
                                <Typography variant="body2" ml={1} data-testid="color">
                                    {formatName(pokemon.color)}
                                </Typography>
                            </Box>
                        </Paper>
                    </Box>
                    
                    <Box mt={3}>
                        <Paper sx={{minHeight: '9rem'}}>
                            <Typography variant="h5" fontWeight="bold">Evolves from:</Typography>
                            {
                                pokemon.evolvesFrom ? 
                                <Box display="flex" justifyContent="center" alignItems="center">
                                    <Box>
                                        <Avatar sx={{height: "5rem", width: "5rem", backgroundColor: "rgba(255, 255, 255, 0.5)"}} src={pokemon.evolvesFrom.sprite!}/>
                                        <Typography textAlign="center" variant="body2" data-testid="description">{formatName(pokemon.evolvesFrom.name)}</Typography>
                                    </Box>
                                    <KeyboardArrowRightIcon sx={{marginBottom: "1rem"}}/>
                                    <Box>
                                        <Avatar sx={{height: "5rem", width: "5rem", backgroundColor: "rgba(255, 255, 255, 0.5)"}} src={pokemon.pokemon.sprite!}/>
                                        <Typography textAlign="center" variant="body2" data-testid="description">{formatName(pokemon.pokemon.name)}</Typography>
                                    </Box>
                                </Box>
                                :
                                <Typography variant="h6" mt={3} textAlign="center" data-testid="evolution">
                                    This Pokemon either has no Evolution or is the first in its chain
                                </Typography>
                            }
                        </Paper>
                    </Box>

                </>)
            }
        </PokemonDetailFrame>
    );
}

export default PokemonDetail;