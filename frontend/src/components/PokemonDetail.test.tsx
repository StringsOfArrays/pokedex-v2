import { render, screen } from "@testing-library/react"
import PokemonDetail from "./PokemonDetail";
import { MockedProvider } from "@apollo/client/testing";
import { getPokemonMocks } from "../graphql/queries/testing/mocks";

const WrappedComponent = () => 
    <MockedProvider mocks={getPokemonMocks} addTypename={false}>
        <PokemonDetail id="1"/>
    </MockedProvider>

describe('PokemonDetail', () => {
    beforeEach(() => {
        render(
            <WrappedComponent />
        );
    });

    it('renders', () => {
        const detailFrame = screen.getByTestId('detail-frame');
        expect(detailFrame).toBeInTheDocument();
    });
    
    it('displays the correct Pokedex No.', async () => {
       expect(await screen.findByText('No. 1 - Test')).toBeInTheDocument();
    });
    
    it('displays the correct type', async () => {
        expect(await screen.findByTestId('normal')).toBeInTheDocument();
    });
    
    it('displayed pokemon has a description', async () => {
        const description = await screen.findByTestId('description');
        expect(description).toHaveTextContent('just a test');
    });
    
    it('displayed pokemon has appearence data', async () => {      
        const weight = await screen.findByTestId('weight');
        const height = await screen.findByTestId('height');
        const color = await screen.findByTestId('color');
    
        expect(weight).toHaveTextContent('Weight - 5lbs');
        expect(height).toHaveTextContent('Height - 0.5m');
        expect(color).toHaveTextContent('Brown');
    });
    
    it('displayed pokemon has no evolution', async () => {
        const evolution = await screen.findByTestId('evolution');
        expect(evolution).toHaveTextContent('This Pokemon either has no Evolution or is the first in its chain');
    });
});