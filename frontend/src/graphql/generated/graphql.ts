/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Evolution = {
  __typename?: 'Evolution';
  name?: Maybe<Scalars['String']>;
  sprite?: Maybe<Scalars['String']>;
};

export type ExtendedPokemon = {
  __typename?: 'ExtendedPokemon';
  color?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  evolvesFrom?: Maybe<Evolution>;
  height?: Maybe<Scalars['Int']>;
  pokemon: Pokemon;
  stats?: Maybe<Array<Maybe<Stat>>>;
  weight?: Maybe<Scalars['Int']>;
};

export type Pokemon = {
  __typename?: 'Pokemon';
  id: Scalars['String'];
  name: Scalars['String'];
  sprite?: Maybe<Scalars['String']>;
  types?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type Query = {
  __typename?: 'Query';
  getAllPokemon: Array<Pokemon>;
  getPokemon: ExtendedPokemon;
};


export type QueryGetPokemonArgs = {
  id?: InputMaybe<Scalars['Int']>;
};

export type Stat = {
  __typename?: 'Stat';
  name?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Int']>;
};

export type GetPokemonQueryVariables = Exact<{
  getPokemonId?: InputMaybe<Scalars['Int']>;
}>;


export type GetPokemonQuery = { __typename?: 'Query', getPokemon: { __typename?: 'ExtendedPokemon', description?: string | null, color?: string | null, height?: number | null, weight?: number | null, stats?: Array<{ __typename?: 'Stat', name?: string | null, value?: number | null } | null> | null, evolvesFrom?: { __typename?: 'Evolution', name?: string | null, sprite?: string | null } | null, pokemon: { __typename?: 'Pokemon', id: string, name: string, types?: Array<string | null> | null, sprite?: string | null } } };

export type GetAllPokemonQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllPokemonQuery = { __typename?: 'Query', getAllPokemon: Array<{ __typename?: 'Pokemon', id: string, name: string, sprite?: string | null, types?: Array<string | null> | null }> };


export const GetPokemonDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetPokemon"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"getPokemonId"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getPokemon"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"getPokemonId"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"stats"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"value"}}]}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"evolvesFrom"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"sprite"}}]}},{"kind":"Field","name":{"kind":"Name","value":"pokemon"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"types"}},{"kind":"Field","name":{"kind":"Name","value":"sprite"}}]}},{"kind":"Field","name":{"kind":"Name","value":"color"}},{"kind":"Field","name":{"kind":"Name","value":"height"}},{"kind":"Field","name":{"kind":"Name","value":"weight"}}]}}]}}]} as unknown as DocumentNode<GetPokemonQuery, GetPokemonQueryVariables>;
export const GetAllPokemonDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetAllPokemon"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"getAllPokemon"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"sprite"}},{"kind":"Field","name":{"kind":"Name","value":"types"}}]}}]}}]} as unknown as DocumentNode<GetAllPokemonQuery, GetAllPokemonQueryVariables>;