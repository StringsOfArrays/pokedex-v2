import { getAllPokemon, getPokemon } from "../pokemonQueries";


export const getPokemonMocks = [
    {
        request: {
          query: getPokemon,
          variables: {getPokemonId: 1}
        },
        result: {
          data: {
            getPokemon: { 
                __typename: 'ExtendedPokemon', 
                description: 'just a test', 
                color: 'brown', 
                height: 5, 
                weight: 5, 
                stats: [
                            {
                                __typename: 'Stat',
                                name: 'a',
                                value: 1,
                            },
                            {
                                __typename: 'Stat',
                                name: 'b',
                                value: 250,
                            },
                            {
                                __typename: 'Stat',
                                name: 'c',
                                value: 8,
                            },
                            {
                                __typename: 'Stat',
                                name: 'd',
                                value: 10,
                            },
                            {
                                __typename: 'Stat',
                                name: 'e',
                                value: 120,
                            },
                            {
                                __typename: 'Stat',
                                name: 'f',
                                value: 190,
                            }
                ], 
                evolvesFrom: null, 
                pokemon: { __typename: 'Pokemon', id: 1, name: 'test', types: ['normal'], sprite: '' } 
            }
          }
        }
      }
];

export const getAllPokemonMocks = [
    {
        request: {
          query: getAllPokemon,
          variables: {}
        },
        result: {
            data: {
                getAllPokemon: [
                        {
                            __typename: "Pokemon",
                            id: '1',
                            name: 'a',
                            types: ['normal'],
                            sprite: '',
                        },
                        {
                            __typename: "Pokemon",
                            id: '3',
                            name: 'c',
                            types: ['normal'],
                            sprite: '',
                        },
                        {
                            __typename: "Pokemon",
                            id: '400',
                            name: 'd',
                            types: ['normal'],
                            sprite: '',
                        },
                        {
                            __typename: "Pokemon",
                            id: '500',
                            name: 'b',
                            types: ['normal', 'poison'],
                            sprite: '',
                        },
                    ]
                }
            }
        }
];