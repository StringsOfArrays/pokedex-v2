import { gql } from "@apollo/client";

export const getPokemon = gql`
    query GetPokemon($getPokemonId: Int) {
        getPokemon(id: $getPokemonId) {
            stats {
                name
                value
            }
            description
            evolvesFrom {
                name
                sprite
            }
            pokemon {
                id
                name
                types
                sprite
            }
            color
            height
            weight
        }
    }
`;

export const getAllPokemon = gql`
    query GetAllPokemon {
        getAllPokemon {
            id
            name
            sprite
            types
        }
    }
`;