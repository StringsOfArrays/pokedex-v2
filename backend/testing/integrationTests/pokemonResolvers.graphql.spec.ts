import { pokemonResolvers } from "../../src/controller/graphql/pokemonController/resolvers";
import { createMockDb } from "../__mocks__/database";
import mongoose from "mongoose";
import { IPokemon, PokemonModel, cache, connectdb, api } from "../../src/model";
import axios from "axios";
import * as validTestPokemon  from "../__mocks__/responses/validPokemon.json";
import * as validSpecies  from "../__mocks__/responses/validSpecies.json";
import { ApolloServer } from "apollo-server-express";
import { loadSchemaSync } from "@graphql-tools/load";
import { join } from "path";
import { GraphQLFileLoader } from "@graphql-tools/graphql-file-loader";

jest.mock('axios');

const schema = loadSchemaSync(join(__dirname, '../../src/controller/graphql/pokemonController/schema.graphql'), { loaders: [new GraphQLFileLoader()] });

describe('pokemonResolvers', () => {
    let mongoServer: any;
    const testPokemon: IPokemon[] = [
        {
            id: '1',
            name: 'bulbasaur',
            types: ['grass'],
            sprite: 'https://bulbasaur-sprite'
        },
        {
            id: '2',
            name: 'ivysaur',
            types: ['grass', 'poison'],
            sprite: 'https://ivysaur-sprite'
        },
        {
            id: '3',
            name: 'venosaur',
            types: ['grass', 'poison'],
            sprite: 'https://venosaur-sprite'
        }
    ];
    
    beforeAll(async () => {
        jest.spyOn(cache, 'set').mockResolvedValue('OK');
        jest.spyOn(cache, 'exists').mockResolvedValue(0);
    });


    describe('getAllPokemon', () => {
        beforeAll(async () => {
            mongoServer = await createMockDb();
            await connectdb();
            await Promise.all(
                testPokemon.map(pkmn => {
                    const pkmnToSave = new PokemonModel(pkmn);
                    return pkmnToSave.save();
                })
            );
        });

        afterAll(async () => {
            await mongoose.disconnect();
            await mongoServer.stop();
        });

        it('returns a list of all saved Pokemon', async () => {
            /*given*/
            const testQuery = `
               query {
                    getAllPokemon {
                        id
                        name
                        types
                        sprite
                    }
                }
            `;

            const expectedResult = {
                getAllPokemon: [
                  {
                    id: '1',
                    name: 'bulbasaur',
                    types: ['grass'],
                    sprite: 'https://bulbasaur-sprite',
                  },
                  {
                    id: '2',
                    name: 'ivysaur',
                    types: ['grass', 'poison'],
                    sprite: 'https://ivysaur-sprite',
                  },
                  {
                    id: '3',
                    name: 'venosaur',
                    types: ['grass', 'poison'],
                    sprite: 'https://venosaur-sprite',
                  },
                ],
              }

            const testServer = new ApolloServer({
                typeDefs: schema,
                resolvers: pokemonResolvers,
            });

            /*when*/
            const response =  await testServer.executeOperation({query: testQuery});
            
            /*then*/
            response?.data?.getAllPokemon.sort((a: { id: any; }, b: { id: any; }) => Number(a.id) - Number(b.id));
            expect(response.data).toMatchObject(expectedResult);
        });
    });

    describe('getPokemon', () => {
        beforeAll(() => {
            (axios.get as jest.MockedFunction<typeof axios.get>).mockImplementation((url) => {
                switch (url) {
                    case `${api}/pokemon/1`:
                        return Promise.resolve(validTestPokemon);
                    case `${api}/pokemon-species/1`:
                        return Promise.resolve(validSpecies);
                    default:
                    return Promise.reject(new Error('not found'));
                }
            });
    
            jest.mock('../../src/controller/graphql/pokemonController/helpers', () => ({
                getEvolvesFrom: jest.fn().mockResolvedValue(null)
            }));
        });
    
        it('returns a valid Pokemon if the id is found', async () => {
            /*given*/
            const testQuery = `
                query {
                    getPokemon(id: 1) {
                    pokemon {
                        id
                        name
                        sprite
                        types
                    }
                    color
                    description
                    evolvesFrom {
                        name
                        sprite
                    }
                    height
                    stats {
                        name
                        value
                    }
                    weight
                    }
                }
            `;

            const expectedResult = {
                  getPokemon: {
                    pokemon: {
                      id: "1",
                      name: "bulbasaur",
                      sprite: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
                      types: [
                        "grass",
                        "poison"
                      ]
                    },
                    color: "green",
                    description: "A strange seed was\nplanted on its\nback at birth.\fThe plant sprouts\nand grows with\nthis POKéMON.",
                    evolvesFrom: null,
                    height: 7,
                    stats: [
                      {
                        name: "hp",
                        value: 45
                      },
                      {
                        name: "attack",
                        value: 49
                      },
                      {
                        name: "defense",
                        value: 49
                      },
                      {
                        name: "special-attack",
                        value: 65
                      },
                      {
                        name: "special-defense",
                        value: 65
                      },
                      {
                        name: "speed",
                        value: 45
                      }
                    ],
                    weight: 69
                  }
              };
            
            const testServer = new ApolloServer({
                typeDefs: schema,
                resolvers: pokemonResolvers,
            });

            /*when*/
            const response =  await testServer.executeOperation({query: testQuery});

            /*then*/
            expect(response.data).toMatchObject(expectedResult);
        });
    
        it('returns an error for an invalid id', async () => {
           
        });
    });
});