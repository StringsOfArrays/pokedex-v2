import { cache, tryGetFromCache } from "../../src/model/redisClient";

describe('tryGetFromCache', () => {
    it('returns null if no cached item has been found', async () => {
        /*given*/
        const exists = jest.spyOn(cache, 'exists').mockResolvedValue(Promise.resolve(0));
        /*when*/
        const cachedValue = await tryGetFromCache('test');
        /*then*/
        expect(cachedValue).toEqual(null);
        expect(exists).toHaveBeenCalledWith('test');
    });

    it('returns cached item if it has been found', async () => {
        /*given*/
        const testData = {
            test: "success"
        }
        jest.spyOn(cache, 'exists').mockResolvedValue(Promise.resolve(1));
        jest.spyOn(cache, 'get').mockResolvedValue(Promise.resolve(JSON.stringify(testData)));
        /*when*/
        const cachedValue = await tryGetFromCache('test');
        /*then*/
        expect(cachedValue).toEqual(testData);
    });
});