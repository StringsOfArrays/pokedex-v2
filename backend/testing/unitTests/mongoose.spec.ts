import mongoose from "mongoose";
import { connectdb } from "../../src/model";
import { createMockDb } from "../__mocks__/database";


describe('connectdb', () => {
    let mongoServer: any;

    beforeEach(async () => {
        mongoServer = await createMockDb();
        jest.spyOn(console, 'error').mockImplementation(() => {});
    });
    
    afterEach(async () => {
        await mongoose.disconnect();
        await mongoServer.stop();
    });

    it('should connect to the database', async () => {
        /*when*/
        await connectdb();
        /*then*/
        expect(mongoose.connection.readyState).toEqual(1);
    });

    it('should should refuse connection to the database with invalid credentials', async () => {
        /*given*/
        process.env.DB_SECRET = 'wrong-password';
        /*when*/
        await connectdb();
        /*then*/
        expect(mongoose.connection.readyState).toEqual(0);
        expect(console.error).toHaveBeenCalled();
    });
});