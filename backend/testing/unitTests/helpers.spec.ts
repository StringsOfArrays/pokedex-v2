import { getEvolvesFrom, getStats, getDescription, Stat, FlavorText } from "../../src/controller/graphql/pokemonController/helpers";
import axios, {AxiosResponse} from "axios";
import {jest} from "@jest/globals";

jest.mock('axios');

describe('getEvolvesFrom', () => {
    let mockResponse: Partial<AxiosResponse>;
    const validSpecies = {
        evolves_from_species: {
            name: "Bulbasaur",
            url: "https://pokeapi.co/api/v2/pokemon/1"
        }
    };

    const invalidSpecies = {}

    beforeEach(() => {
        mockResponse = {
            data: {
                sprites: {
                    front_default:"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
                }
            },
            status: 200,
            statusText: 'OK',
            headers: {}
        };

        (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValue(mockResponse);
    });

    it('can retrieve the name of its pre-evolution, if it has one', async () => {
        /*when*/
        const evolvesFrom = await getEvolvesFrom(validSpecies);
        /*then*/
        expect(evolvesFrom?.sprite).toEqual(mockResponse.data.sprites.front_default);
    });

    it('returns null if the species has no pre-evolution', async () => {
        /*when*/
        const evolvesFrom = await getEvolvesFrom(invalidSpecies);
        /*then*/
        expect(evolvesFrom).toEqual(null);
    })
});

describe('getStats', () => {
    const testStats: Stat[] = [
        {
            base_stat: 20,
            effort: 0,
            stat: {
                name: "hp",
                url:  "https://pokeapi.co/api/v2/",
            }
        },
        {
            base_stat: 25,
            effort: 0,
            stat: {
                name: "atk",
                url:  "https://pokeapi.co/api/v2/",
            }
        },
        {
            base_stat: 30,
            effort: 0,
            stat: {
                name: "def",
                url:  "https://pokeapi.co/api/v2/",
            }
        },
    ];

    it('can format stats accordingly', () => {
        /*given*/
        const expectedStats: GQL.IStat[] = [
            {__typename: "Stat", name: "hp", value: 20},
            {__typename: "Stat", name: "atk", value: 25},
            {__typename: "Stat", name: "def", value: 30},
        ];
        /*when*/
        const stats = getStats(testStats);
        /*then*/
        expect(stats).toEqual(expectedStats);
    })
});

describe('getDescription', () => {
    const testFlavorTexts: FlavorText[] = [
        {
            flavor_text: "Una rara semilla le fue plantada en el lomo al nacer.\nLa planta brota y crece con este Pokémon.",
            language: {
            name: "es",
            url: "https://pokeapi.co/api/v2/language/7/"
            }
        },
        {
            flavor_text: "A strange seed was\nplanted on its\nback at birth.\fThe plant sprouts\nand grows with\nthis POKéMON.",
            language: {
            name: "en",
            url: "https://pokeapi.co/api/v2/language/9/"
            }
        },
        {
            flavor_text: "A strange seed was\nplanted on its\nback at birth.\fThe plant sprouts\nand grows with\nthis POKéMON.",
            language: {
            name: "en",
            url: "https://pokeapi.co/api/v2/language/9/"
            }
        },
        {
            flavor_text: "Una rara semilla le fue plantada en el lomo al nacer.\nLa planta brota y crece con este Pokémon.",
            language: {
            name: "es",
            url: "https://pokeapi.co/api/v2/language/7/"
            }
        }
    ];

    it("extracts the first found english flavor text", () => {
        /*given*/
        const expectedDescription = "A strange seed was\nplanted on its\nback at birth.\fThe plant sprouts\nand grows with\nthis POKéMON.";
        /*when*/
        const description = getDescription(testFlavorTexts);
        /*then*/
        expect(description).toEqual(expectedDescription);
    });
});