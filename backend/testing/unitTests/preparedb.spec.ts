import axios from "axios"
import { api, connectdb, PokemonModel, prepareDb } from "../../src/model";
import { createMockDb } from "../__mocks__/database";
import mongoose from "mongoose";
import * as pokemon1 from '../__mocks__/responses/pokemon-1.json';
import * as pokemon2 from '../__mocks__/responses/pokemon-2.json';
import * as pokemon3 from '../__mocks__/responses/pokemon-3.json';

jest.mock('axios');

describe('preparedb', () => {
    const allPokemon = {
        count: 3,
        next: null,
        previous: null,
        results: [
            {name:"bulbasaur",
            url:"https://pokeapi.co/api/v2/pokemon/1/"},
            {name:"ivysaur",
            url:"https://pokeapi.co/api/v2/pokemon/2/"},
            {name:"venusaur",
            url:"https://pokeapi.co/api/v2/pokemon/3/"},
        ]
    };

    let mongoServer: any;

    beforeAll(async () => {
        (axios.get as jest.MockedFunction<typeof axios.get>).mockImplementation((url) => {
            switch (url) {
                case `${api}/pokemon?limit=100000&offset=0`:
                    return Promise.resolve({data: allPokemon});
                case 'https://pokeapi.co/api/v2/pokemon/1/':
                    return Promise.resolve({data: pokemon1});
                case 'https://pokeapi.co/api/v2/pokemon/2/':
                    return Promise.resolve({data: pokemon2});
                case 'https://pokeapi.co/api/v2/pokemon/3/':
                    return Promise.resolve({data: pokemon3});
                default:
                return Promise.reject(new Error('not found'))
            }
        });
        mongoServer = await createMockDb();
        await connectdb();
    });

    afterAll(async () => {
        await mongoose.disconnect();
        await mongoServer.stop();
    });

    it('can recursively generate Database entries', async () => {
        /*given*/
        const expctedResult = [
            {
                id: "1",
                name: "bulbasaur",
                sprite: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
                types: ["grass", "poison"]
            },
            {
                id: "2",
                name: "ivysaur",
                sprite: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png",
                types: ["grass", "poison"]
            },
            {
                id: "3",
                name: "venusaur",
                sprite: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png",
                types: ["grass", "poison"]
            },
        ];
        /*when*/
        await prepareDb();
        /*then*/
        await new Promise<void>((resolve) => {
          setTimeout(async () => {
            const fromDb = await PokemonModel.find({}).lean();
            expect(fromDb.length).toEqual(allPokemon.results.length);
            expect(fromDb.sort((a, b) => Number(a.id) - Number(b.id))).toMatchObject(expctedResult);
            resolve();
          }, 10000);
        });
    }, 15000);
})