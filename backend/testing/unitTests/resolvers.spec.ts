import { pokemonResolvers } from "../../src/controller/graphql/pokemonController/resolvers";
import { createMockDb } from "../__mocks__/database";
import mongoose from "mongoose";
import { IPokemon, PokemonModel, cache, connectdb, api } from "../../src/model";
import axios from "axios";
import * as validTestPokemon  from "../__mocks__/responses/validPokemon.json";
import * as validSpecies  from "../__mocks__/responses/validSpecies.json";

jest.mock('axios');

describe('pokemonResolvers', () => {
    let mongoServer: any;
    const testPokemon: IPokemon[] = [
        {
            id: '1',
            name: 'bulbasaur',
            types: ['grass'],
            sprite: 'https://bulbasaur-sprite'
        },
        {
            id: '2',
            name: 'ivysaur',
            types: ['grass', 'poison'],
            sprite: 'https://ivysaur-sprite'
        },
        {
            id: '3',
            name: 'venosaur',
            types: ['grass', 'poison'],
            sprite: 'https://venosaur-sprite'
        }
    ];
    
    beforeAll(() => {
        jest.spyOn(cache, 'set').mockResolvedValue('OK');
        jest.spyOn(cache, 'exists').mockResolvedValue(0);
    });

    describe('getAllPokemon', () => {
        beforeAll(async () => {
            mongoServer = await createMockDb();
            connectdb();
            await Promise.all(
                testPokemon.map(pkmn => {
                    const pkmnToSave = new PokemonModel(pkmn);
                    return pkmnToSave.save();
                })
            );  
        });

        afterAll(async () => {
            await mongoose.disconnect();
            await mongoServer.stop();
        });

        it('returns a list of all saved Pokemon', async () => {
            /*when*/
            const response =  await pokemonResolvers.Query.getAllPokemon(null, null, {}, null);
            /*then*/
            response.sort((a: { id: any; }, b: { id: any;}) => Number(a.id) - Number(b.id));
            response.forEach((res: any, i: number) => {
                expect(res).toMatchObject(testPokemon[i]);
            });
        });
    });

    describe('getPokemon', () => {
        beforeAll(() => {
            (axios.get as jest.MockedFunction<typeof axios.get>).mockImplementation((url) => {
                switch (url) {
                    case `${api}/pokemon/1`:
                        return Promise.resolve(validTestPokemon);
                    case `${api}/pokemon-species/1`:
                        return Promise.resolve(validSpecies);
                    default:
                    return Promise.reject(new Error('not found'))
                }
            });

            jest.mock('../../src/controller/graphql/pokemonController/helpers', () => ({
                getEvolvesFrom: jest.fn().mockResolvedValue(null)
            }));
        });

        it('returns a valid Pokemon if the id is found', async () => {
            /*given*/
            const expectedPokemon =  {
                pokemon: {
                    id: 1,
                    name: 'bulbasaur',
                    types: [ 'grass', 'poison' ],
                    sprite: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
                    __typename: 'Pokemon'
                },
                __typename: 'ExtendedPokemon',
                color: 'green',
                height: 7,
                weight: 69,
                stats: [
                    { name: 'hp', value: 45, __typename: 'Stat' },
                    { name: 'attack', value: 49, __typename: 'Stat' },
                    { name: 'defense', value: 49, __typename: 'Stat' },
                    { name: 'special-attack', value: 65, __typename: 'Stat' },
                    { name: 'special-defense', value: 65, __typename: 'Stat' },
                    { name: 'speed', value: 45, __typename: 'Stat' }
                ],
                description: 'A strange seed was\n' +
                    'planted on its\n' +
                    'back at birth.\fThe plant sprouts\n' +
                    'and grows with\n' +
                    'this POKéMON.',
                evolvesFrom: null
            }
            
            /*when*/
            const pokemon = await pokemonResolvers.Query.getPokemon(null, {id: 1}, {}, null);
            /*then*/
            expect(pokemon).toEqual(expectedPokemon);
        });

        it('returns an error for an invalid id', async () => {
            /*given*/
            const expectedError = { message: 'not found' };
            /*when*/
            const invalidPokemon = await pokemonResolvers.Query.getPokemon(null, {id: 800000}, {}, null);
            /*then*/
            expect(invalidPokemon).toEqual(expectedError);
        });
    });
});