import { createMockDb } from '../__mocks__/database'
import { connectdb } from "../../src/model";
import { IPokemon, PokemonModel, savePokemon } from "../../src/model/pokemonModel";
import mongoose from "mongoose";

describe('savePokemon', () => {
    let mongoServer: any;

    beforeEach(async () => {
        mongoServer = await createMockDb();
        await connectdb();
    });

    afterEach(async () => {
        await mongoose.disconnect();
        await mongoServer.stop();
    });

    it('successfully saves a Pokemon to the db', async () => {
        /*given*/
        const testPokemon: IPokemon = {
            id: "1",
            name: "Bulbasaur",
            types: ["grass"],
            sprite: "https://fancy-sprite"
        }
        /*when*/
        await savePokemon(testPokemon);
        /*then*/
        const pokemon = await PokemonModel.find({id: "1"});
        expect(pokemon[0]).toMatchObject(testPokemon);
        
    });
});