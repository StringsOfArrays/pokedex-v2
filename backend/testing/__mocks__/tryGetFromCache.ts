import {jest} from '@jest/globals';
import {tryGetFromCache} from '../../src/model/redisClient'

const mock = jest.fn<typeof tryGetFromCache>().mockResolvedValue(null);
export {mock as tryGetFromCache};