import { MongoMemoryServer } from "mongodb-memory-server";

export const createMockDb = async () => {
    process.env.DB_USER = 'testuser';
    process.env.DB_SECRET = 'testpassword';
    const mongoServer = await MongoMemoryServer.create({
        instance: {
            auth: true
        },
        auth: {
            customRootName: process.env.DB_USER,
            customRootPwd: process.env.DB_SECRET
        }
    });
    const mongoUri = mongoServer.getUri();
    process.env.DB_HOST = mongoUri.replace('mongodb://', '').replace('/', '');
    return mongoServer;
}