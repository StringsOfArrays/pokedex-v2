type Type = {slot: number, type: {name: string, url: string}};

export const getTypes = (pokemonTypes: Type[]): string[] => {
    return pokemonTypes.map((type: {slot: number, type: {name: string, url: string}}) => type.type.name);
}