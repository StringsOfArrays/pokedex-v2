import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { connectdb } from './model';
import { loadSchemaSync } from '@graphql-tools/load';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { join } from 'path';
import { prepareDb } from './model/preparedb';
import { pokemonResolvers } from './controller/graphql/pokemonController';
import graphqlPlayground from 'graphql-playground-middleware-express';

const schema = loadSchemaSync(join(__dirname, '/controller/graphql/pokemonController/schema.graphql'), { loaders: [new GraphQLFileLoader()] });

const app = express();

(async () => {
  const server = new ApolloServer({
    typeDefs: schema,
    resolvers: pokemonResolvers,
    introspection: true
  });
  await server.start();

  server.applyMiddleware({app, path: '/graphql'});
  app.get('/playground', graphqlPlayground({ endpoint: '/graphql' }));

  connectdb()
  .catch()
  .then(() => {
    prepareDb();
  })
  .then(() => {
      app.listen(1337);
      console.log('Running a GraphQL API server at http://localhost:1337/graphql');
  });
})();


