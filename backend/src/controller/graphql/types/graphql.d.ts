// tslint:disable
// graphql typescript definitions

declare namespace GQL {
interface IGraphQLResponseRoot {
data?: IQuery;
errors?: Array<IGraphQLResponseError>;
}

interface IGraphQLResponseError {
/** Required for all errors */
message: string;
locations?: Array<IGraphQLResponseErrorLocation>;
/** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
[propName: string]: any;
}

interface IGraphQLResponseErrorLocation {
line: number;
column: number;
}

interface IQuery {
__typename: "Query";
getPokemon: IExtendedPokemon;
getAllPokemon: Array<IPokemon>;
}

interface IGetPokemonOnQueryArguments {
id?: number | null;
}

interface IExtendedPokemon {
__typename: "ExtendedPokemon";
pokemon: IPokemon;
color: string | null;
height: number | null;
weight: number | null;
evolvesFrom: IEvolution | null;
stats: Array<IStat | null> | null;
description: string | null;
}

interface IPokemon {
__typename: "Pokemon";
id: string;
name: string;
types: Array<string | null> | null;
sprite: string | null;
}

interface IEvolution {
__typename: "Evolution";
name: string | null;
sprite: string | null;
}

interface IStat {
__typename: "Stat";
name: string | null;
value: number | null;
}
}

// tslint:enable
