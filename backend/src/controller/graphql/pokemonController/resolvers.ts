import axios from "axios";
import { api, PokemonModel } from "../../../model";
import { cache, tryGetFromCache } from "../../../model/redisClient";
import { getTypes } from "../../../shared/pokemonFormatters";
import { Resolvers } from "../types/graphql-utils";
import { getDescription, getEvolvesFrom, getStats } from "./helpers";

export const pokemonResolvers: Resolvers = {
    Query: {
        getPokemon: async (_parent, { id }: GQL.IGetPokemonOnQueryArguments) => {
            try {
                // Check cache first
                const cacheKey = `pokemon:dexno:${id}`;
                const pokemonFromCache = await tryGetFromCache(cacheKey);
                if(pokemonFromCache) {
                    return pokemonFromCache as GQL.IExtendedPokemon;
                }
    
                // Base Pokemon
                const pokemonBase = (await axios.get(`${api}/pokemon/${id}`)).data;
                const types = getTypes(pokemonBase.types);
                const sprite = pokemonBase.sprites.front_default;
                const stats = getStats(pokemonBase.stats);
    
                // Extended Species Data
                const species = (await axios.get(`${api}/pokemon-species/${id}`)).data;
                const evolvesFrom = await getEvolvesFrom(species);
                const description = getDescription(species.flavor_text_entries);
    
                const pokemon: GQL.IPokemon = {
                    id: pokemonBase.id,
                    name: pokemonBase.name,
                    types,
                    sprite,
                    __typename: "Pokemon"
                }
    
                const extendedPokemon: GQL.IExtendedPokemon = {
                    pokemon,
                    __typename: "ExtendedPokemon",
                    color: species.color.name,
                    height: pokemonBase.height,
                    weight: pokemonBase.weight,
                    stats,
                    description,
                    evolvesFrom
                }
    
                await cache.set(cacheKey, JSON.stringify(extendedPokemon));
                return extendedPokemon as GQL.IExtendedPokemon;
            } catch(err) {
                const errors: GQL.IGraphQLResponseError = {
                    message: err.message
                }
                return errors;
            }
        },
        getAllPokemon: async () => {
            try {
                const cacheKey = 'allPokemon';
                // Try cache first
                const allPokemonFromCache = await tryGetFromCache(cacheKey);
                if(allPokemonFromCache) { 
                    return allPokemonFromCache as GQL.IPokemon[];
                }
    
                const allPokemon = (await PokemonModel.find({}).lean()).filter(pkmn => !!pkmn.sprite && Number(pkmn.id) < 10000).map(pkmn => ({...pkmn, __typename: "Pokemon"}));
                await cache.set(cacheKey, JSON.stringify(allPokemon));
                return allPokemon as GQL.IPokemon[];
            } catch(err) {
                const errors: GQL.IGraphQLResponseError = {
                    message: err.message
                }
                return errors;
            }
        },
    }
};