import axios from "axios";
import { api } from "../../../model";

export type Stat = {base_stat: number, effort: number, stat: {name: string, url: string}};
export type FlavorText = {flavor_text: string, language: {name: string, url: string}};

export const getStats = (pokemonStats: Stat[]): GQL.IStat[] => {
    return pokemonStats.map(pokemonStat => ({name: pokemonStat.stat.name, value: pokemonStat.base_stat, __typename: "Stat"}));
}

export const getDescription = (flavorTextEntries: FlavorText[]): string | null => {
    return flavorTextEntries.find(entry => entry.language.name === 'en')?.flavor_text ?? null;
}

export const getEvolvesFrom = async (species: any): Promise<GQL.IEvolution | null> => {
    const hasEvolutions = species.evolves_from_species;
    if(!hasEvolutions) {
        return null;
    }
    const { name } = species.evolves_from_species;
    const pokemonBase = (await axios.get(`${api}/pokemon/${name}`)).data;
    const sprite = pokemonBase.sprites.front_default;
    const evolvesFrom: GQL.IEvolution = {
        name, sprite,
        __typename: "Evolution"
    };
    return evolvesFrom;
}