import Redis from "ioredis";
export const cache = new Redis(`redis://:${process.env.REDIS_PASSWORD}@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`);

export const tryGetFromCache = async (cacheKey: string) => {
    const exists = await cache.exists(cacheKey);
    if(exists === 0) {
        return null;
    }
    const value = await cache.get(cacheKey);
    return JSON.parse(value!);
}