import { Schema, model } from 'mongoose';

export interface IPokemon {
    id: string;
    name: string;
    types: string[];
    sprite: string;
}

const pokemonSchema = new Schema<IPokemon>({
    id: { type: String, required: true },
    name: { type: String, required: true },
    types: { type: Schema.Types.Mixed, required: true },
    sprite: {type: String, required: true}
});

const Pokemon = model<IPokemon>('Pokemon', pokemonSchema);

export const savePokemon = async (pkmn: IPokemon) => {
    await Pokemon.findOneAndUpdate({name: pkmn.name}, pkmn, {upsert: true});
    console.log(`Pokemon: ${pkmn.name} - has been cached`);
}

export {Pokemon as PokemonModel};