import { connect } from "mongoose";

export const connectdb = async () => {
    try {
        console.log(`mongodb://${process.env.DB_USER}:${process.env.DB_SECRET}@${process.env.DB_HOST}`);
        await connect(`mongodb://${process.env.DB_USER}:${process.env.DB_SECRET}@${process.env.DB_HOST}/pokemondb?authSource=admin`);
        console.log("Database connected");
    } catch(error) {
        console.error(`Database connection failed! Reason: ${error}`);
    }
}