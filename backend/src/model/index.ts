export { savePokemon, PokemonModel, IPokemon } from "./pokemonModel";
export { connectdb } from "./mongoose";
export { cache, tryGetFromCache } from './redisClient';
export { prepareDb } from "./preparedb";
export const api = "https://pokeapi.co/api/v2";