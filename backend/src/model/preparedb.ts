import axios from "axios";
import { PokemonModel, savePokemon, IPokemon, api } from ".";
import { cache, tryGetFromCache } from "./redisClient";

let i = 0;
let pokeUrls: Array<{name: string, url: string}> = [];

const getPokemonUrls = async () => {
    const res = await axios.get(`${api}/pokemon?limit=100000&offset=0`);
    pokeUrls = res.data.results as Array<{name: string, url: string}>;
}

const checkForUpdate = async () => {
    if(process.env.ONLY_CHECK_FOR_NEW_POKEMON !== 'true') {
        return;
    }

    const lastEntry = (await PokemonModel.find({}).sort({_id: -1}).limit(1))[0];
    if(lastEntry) {
        const index = pokeUrls.findIndex(element => element.name === lastEntry.name);
        i = index >= 0 ? index + 1 : 0;
    }
}

export const prepareDb = async () => {
    // Dev settings
    if(process.env.NODE_ENV === 'development' && process.env.SKIP_DB === 'true') {
        return;
    }

    // 1. Get the last entry to not start the process from the beginning if not necessary
    if(i == 0) {
        await getPokemonUrls();
        await checkForUpdate();
    }

    // 2. Check for exit condition, once every Pokemon has been cached
    if(i >= pokeUrls.length) {
        console.log('----- All Pokemon have been cached to Databse -----');
        return;
    }

    // 3. Get the current Pokemon and write it to db
    const url = pokeUrls[i].url;

    // ... but first check the cache
    const pokemonFromCache = await tryGetFromCache(url);
    if(pokemonFromCache) {
        await savePokemon(pokemonFromCache);
    } else {
        const res = await axios.get(url) as any;
        const types = res.data.types.map((type: {slot: number, type: {name: string, url: string}}) => type.type.name); 
        const pokemon: IPokemon = {
            id: res.data.id,
            name: res.data.name,
            sprite: res.data.sprites.front_default,
            types
        }
        await cache.set(url, JSON.stringify(pokemon));
        await savePokemon(pokemon);
    }

    
    // 4. Adjust iterator and start the process over
    // NOTE: PokeApi is rate limited, therefore we give it some space
    i += 1;
    const timeout = (i !== 1 && i % 10 === 1) ? 10000 : 3000;
    if(timeout > 3000) {
        console.log("--- Pause ---");
    }
    setTimeout(() => {prepareDb()}, timeout);
}